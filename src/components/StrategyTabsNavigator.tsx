import React from 'react';
import {makeStyles, Theme} from '@material-ui/core/styles';
import {AppBar, Paper, Tabs, Tab} from '@material-ui/core';
import styled from 'styled-components';

import StrategyCompetitionsCurrentMonthTable from './StrategyCompetitionsCurrentMonthTable';
import StrategyCompetitionsFutureTable from "./StrategyCompetitionsFutureTable";


export default function StrategyTabsNavigator() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    return (
        <Paper className={classes.root}>
            <AppBar position="static">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    centered
                >
                    <Tab label="Strategies sent this month"/>
                    <Tab label="Strategies that participate in competitions"/>
                    <Tab label="Completed Competitions"/>
                    <Tab label="All Strategies"/>
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                <StrategyCompetitionsCurrentMonthTable/>
            </TabPanel>
            <TabPanel value={value} index={1}>
                <StrategyCompetitionsFutureTable/>
            </TabPanel>
            <TabPanel value={value} index={2}>
                Item Three
            </TabPanel>
            <TabPanel value={value} index={3}>
                Item 4
            </TabPanel>
        </Paper>
    );
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
}));

interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}

const TabDiv = styled.div`
padding-top: 2%;
`;

function TabPanel(props: TabPanelProps) {
    const {children, value, index,} = props;

    return (
        <TabDiv
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
        >
            {children}
        </TabDiv>
    );
}