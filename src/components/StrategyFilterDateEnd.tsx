import React from 'react'
import {FormControl, FormLabel, FormGroup, FormControlLabel, Switch} from "@material-ui/core";
import SelectorHelper from "./SelectorHelper";

type StrategyFilterDateEndProps = {
    dateEndDefault: any,
    handleChangeShowDateEnd: any
}

export default class StrategyFilterDuration extends React.Component<StrategyFilterDateEndProps> {
    constructor(props: any) {
        super(props);

        this.state = {
            dateEndForChoice: SelectorHelper.getForChoice(props.dateEndDefault)
        }
    }

    handleChangeShowDateEnd(dateEnd: string) {
        const newState = [];
        // @ts-ignore
        for (const obj of this.state.dateEndForChoice) {
            if (obj.value === dateEnd) {
                newState.push({
                    value: obj.value,
                    isChoice: !obj.isChoice
                });
            } else {
                newState.push(obj);
            }
        }

        this.setState({
            dateEndForChoice: newState
        });
        const dateEndChoices = SelectorHelper.getForChoiceRevertIfTrue(newState);
        // @ts-ignore
        this.props.handleChangeShowDateEnd(dateEndChoices);
    }

    render() {
        return (
            <FormControl component="fieldset">
                <FormLabel component="legend" color="primary">Date Contest End:</FormLabel>

                <FormGroup>
                    {
                        // @ts-ignore
                        this.state.dateEndForChoice.map(dateEndObj => {
                            const dateEnd = dateEndObj.value;
                            return (
                                <FormControlLabel key={dateEnd}
                                                  control={<Switch name={dateEnd.toString()}
                                                                   onChange={() => this.handleChangeShowDateEnd(dateEnd)}
                                                                   checked={dateEndObj.isChoice}
                                                                   color="primary"
                                                  />}
                                                  label={dateEnd}/>)
                        })
                    }
                </FormGroup>
            </FormControl>
        )
    }
}