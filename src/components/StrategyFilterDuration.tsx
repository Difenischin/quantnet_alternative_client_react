import React from 'react'
import {FormLabel, FormControl, FormGroup, FormControlLabel, Switch} from '@material-ui/core';
import SelectorHelper from "./SelectorHelper";

type StrategyFilterDurationProps = {
    durationsDefault: any,
    handleChangeShowDurations: any
}

export default class StrategyFilterDuration extends React.Component<StrategyFilterDurationProps> {

    constructor(props: any) {
        super(props);

        this.state = {
            durationsDefault: props.durationsDefault,
            durationsForChoice: SelectorHelper.getForChoice(props.durationsDefault)
        }
    }

    handleChangeShowDurations(duration: number) {
        const newState = [];
        // @ts-ignore
        for (const obj of this.state.durationsForChoice) {
            if (obj.value === duration) {
                newState.push({
                    value: obj.value,
                    isChoice: !obj.isChoice
                });
            } else {
                newState.push(obj);
            }
        }

        this.setState({
            durationsForChoice: newState
        });
        const durationsChoices = SelectorHelper.getForChoiceRevertIfTrue(newState);
        // @ts-ignore
        this.props.handleChangeShowDurations(durationsChoices);
    }

    render() {
        return (
            <div>
                <FormControl component="fieldset">
                    <FormLabel component="legend" color="primary">Choice shows durations</FormLabel>

                    <FormGroup>
                        {
                            // @ts-ignore
                            this.state.durationsForChoice.map(durationObj => {
                                const duration = durationObj.value;
                                return (
                                    <FormControlLabel key={duration}
                                                      control={<Switch name={duration.toString()}
                                                                       onChange={() => this.handleChangeShowDurations(duration)}
                                                                       checked={durationObj.isChoice}
                                                                       color="primary"
                                                      />}
                                                      label={duration}/>)
                            })
                        }
                    </FormGroup>
                </FormControl>
            </div>
        )
    }
}