import React from 'react';
import './App.css';
import StrategyTabsNavigator from './components/StrategyTabsNavigator';

function App() {

    return (
        <div className="App">
            {/*<header className="App-header">*/}
            {/*    <img src="/quantnet_logo.png"/>*/}
            {/*</header>*/}
            <StrategyTabsNavigator></StrategyTabsNavigator>

        </div>
    );
}

export default App;
